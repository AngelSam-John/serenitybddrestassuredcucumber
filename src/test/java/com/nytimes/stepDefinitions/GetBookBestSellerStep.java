package com.nytimes.stepDefinitions;

import com.nytimes.common.CommonRequestSpecification;
import com.nytimes.common.CommonResponseValidation;
import com.nytimes.common.ConfigReader;
import com.nytimes.endPoints.bestSeller.BookBestSellerRequest;
import com.nytimes.endPoints.bestSeller.BookBestSellerResponse;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;

public class GetBookBestSellerStep {

    ConfigReader configReader = new ConfigReader();
    String apiKey = configReader.getAPIKey();
    String invalidApiKey = configReader.getInvalidAPIKey();

    @Steps
    CommonRequestSpecification commonRequestSpecification;

    @Steps
    CommonResponseValidation commonResponseValidation;

    @Steps
    BookBestSellerRequest bookBestSellerRequest;

    @Steps
    BookBestSellerResponse bookBestSellerResponse;

    @Given("Book BestSeller API is available to user")
    public void bookBestSellerAPIIsAvailableToUser() {
        commonRequestSpecification.getBookBestSellerSpecification();
    }

    @When("User requests for best seller of a book with {string} key")
    public void userRequestsForBestSellerOfABookWithBookDetailsAndApiKey(String isValidOrInvalidKey, DataTable dataTable) {
        List<Map<String, String>> bookList = dataTable.asMaps(String.class, String.class);

        for (Map<String, String> book : bookList) {
            String author = book.get("Author");
            String publisher = book.get("Publisher");
            String title = book.get("Title");
            if (isValidOrInvalidKey.equalsIgnoreCase("valid"))
                bookBestSellerRequest.getBookBestSeller(author, publisher, title, apiKey);
            else
                bookBestSellerRequest.getBookBestSeller(author, publisher, title, invalidApiKey);
            commonResponseValidation.printOutputLog(lastResponse());
        }
    }

    @Then("User receives Best Seller of the given book with author")
    public void userReceivesBestSellerOfTheGivenBookIsPresent(DataTable dataTable) {
        List<Map<String, String>> bookAuthorList = dataTable.asMaps(String.class, String.class);

        for (Map<String, String> bookAuthor : bookAuthorList) {
            String author = bookAuthor.get("Author");
            String actualAuthor = bookBestSellerResponse.getAuthor(lastResponse());
            commonResponseValidation.validateContentIsSame(author, actualAuthor);
        }
    }
}
