package com.nytimes.stepDefinitions;

import com.nytimes.common.CommonRequestSpecification;
import com.nytimes.endPoints.bookReview.BookReviewRequest;
import com.nytimes.endPoints.bookReview.BookReviewResponse;
import com.nytimes.common.CommonResponseValidation;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class GetBookReviewStep {

    @Steps
    CommonResponseValidation commonResponse;

    @Steps
    BookReviewResponse bookReviewResponseValidation;

    @Steps
    BookReviewRequest bookReviewRequest;

    @Steps
    CommonRequestSpecification commonRequestSpecification;

    @Given("Book API is available to user")
    public void bookReviewAPIIsAvailable() {
        commonRequestSpecification.getBookReviewsSpecification();
    }

    @When("User requests review endpoint with book details")
    public void userEntersBookDetails(DataTable dataTable) {
        List<Map<String, String>> bookList = dataTable.asMaps(String.class, String.class);

        for (Map<String, String> book : bookList) {
            String isbn = book.get("ISBN");
            String title = book.get("Title");
            String author = book.get("Author");
            bookReviewRequest.getBookReview(isbn, title, author);
            commonResponse.printOutputLog(lastResponse());
        }
    }

    @Then("User validates response code is {int}")
    public void validateResponseCodeAs(int response) {
        commonResponse.responseCodeIs(response, lastResponse());
    }

    @Then("User validates the response has summary details")
    public void reviewForTheGivenBookShouldBeDisplayed() {
        String summaryDetails = bookReviewResponseValidation.getSummary(lastResponse());
        commonResponse.validateContentNotNull(summaryDetails);
    }

    @And("User validates title is present in the response")
    public void titleAsShouldBeDisplayed(DataTable dataTable) {
        List<Map<String, String>> bookTitleList = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> title : bookTitleList) {
            String bookTitle = title.get("Title");
            String actualTitle = bookReviewResponseValidation.getTitle(lastResponse());
            commonResponse.validateContentIsSame(bookTitle, actualTitle);
        }
    }

    @And("User validates author is present in the response")
    public void authorShouldBePresent(DataTable dataTable) {
        List<Map<String, String>> bookAuthorList = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> author : bookAuthorList) {
            String bookAuthor = author.get("Author");
            String actualAuthor = bookReviewResponseValidation.getAuthor(lastResponse());
            commonResponse.validateContentIsSame(bookAuthor, actualAuthor);
        }
    }

    @Then("User validates response schema matches with the specification defined in {string}")
    public void theSchemaShouldMatchWithTheSpecificationDefinedIn(String spec) {
        commonResponse.validateJsonSchema(lastResponse(), spec);
    }
}
