package com.nytimes.common;

import com.nytimes.endPoints.bestSeller.BookBestSellerRequest;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonRequestSpecification {
    private static final Logger logger = LoggerFactory.getLogger(CommonRequestSpecification.class);

    @Step("Build Book Request Specification")
    public static RequestSpecification bookRequestSpecification() {
        EnvironmentVariables environmentVariables = Injectors.getInjector()
                .getInstance(EnvironmentVariables.class);

        String baseUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("baseurl");

        return new RequestSpecBuilder().setBaseUri(baseUrl)
                .setContentType("application/json")
                .build();
    }

    @Step("Request Specification for Book Reviews is Set")
    public RequestSpecification getBookReviewsSpecification() {
        logger.info("Setting Request Specification for Book Review EndPoint!");
        return SerenityRest
                .given()
                .spec(bookRequestSpecification())
                .basePath(Route.getReviews());

    }

    @Step("Request Specification for Book BestSeller is Set")
    public RequestSpecification getBookBestSellerSpecification() {
        logger.info("Setting Request Specification for Book BestSeller EndPoint!");
        return SerenityRest
                .given()
                .spec(bookRequestSpecification())
                .basePath(Route.getBestseller());

    }

}
