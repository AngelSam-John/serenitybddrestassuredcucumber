package com.nytimes.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

public class ConfigReader {

    private static final Logger logger = LoggerFactory.getLogger(ConfigReader.class);
    public Properties properties;
    private final String propertyFilePath = "src/test/resources/configuration/Configuration.properties";


    public ConfigReader() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            logger.error("Failed to read the properties from file {}", e);
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }


    public String getAPIKey() {
        String apiKey = properties.getProperty("API_KEY");
        if (apiKey != null) return apiKey;
        else throw new RuntimeException("API_KEY not specified in the Configuration.properties file.");
    }

    public String getInvalidAPIKey() {
        String apiKey = properties.getProperty("INVALID_KEY");
        if (apiKey != null) return apiKey;
        else throw new RuntimeException("INVALID_KEY not specified in the Configuration.properties file.");
    }
}
