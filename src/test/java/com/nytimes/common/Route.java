package com.nytimes.common;

public class Route {
    private static final String BESTSELLER = "/lists/best-sellers/history.json";
    private static final String REVIEWS = "/reviews.json";

    public static String getBestseller() {
        return BESTSELLER ;
    }

    public static String getReviews() {
        return REVIEWS;
    }
}
