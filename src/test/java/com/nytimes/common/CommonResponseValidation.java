package com.nytimes.common;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;


public class CommonResponseValidation {

    private static final Logger logger = LoggerFactory.getLogger(CommonResponseValidation.class);

    public void printOutputLog(Response response) {
        response.then().log().all();
    }

    @Step("Verify that API response code is {0}")
    public void responseCodeIs(int responseCode, Response lastResponse) {
        logger.info("Validating Response Code from the response...", responseCode);
        assertThat(lastResponse.statusCode()).isEqualTo(responseCode);
        logger.info("Validation of Response Code is successful!", responseCode);
    }

    @Step("Verify that Schema is valid")
    public void validateJsonSchema(Response lastResponse, String schemaJson) {
        logger.info("Validating JSON Schema...", schemaJson);
        lastResponse
                .then()
                .body(matchesJsonSchemaInClasspath("schema/" + schemaJson));
        logger.info("Validation of JSON Schema is successful!");

    }

    @Step("Verify contents are the same")
    public void validateContentIsSame(String expected, String actual) {
        Assert.assertEquals(expected, actual);
    }

    @Step("Verify content is not null")
    public void validateContentNotNull(String message){
        Assert.assertNotNull(message);
    }

}
