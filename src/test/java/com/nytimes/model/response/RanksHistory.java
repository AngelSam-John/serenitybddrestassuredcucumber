package com.nytimes.model.response;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RanksHistory {

    private String primaryIsbn10;
    private String primaryIsbn13;
    private Integer rank;
    private String listName;
    private String displayName;
    private String publishedDate;
    private String bestsellersDate;
    private Integer weeksOnList;
    private Integer rankLastWeek;
    private Integer asterisk;
    private Integer dagger;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public RanksHistory() {
    }

    /**
     *
     * @param primaryIsbn13
     * @param dagger
     * @param displayName
     * @param asterisk
     * @param rank
     * @param listName
     * @param publishedDate
     * @param primaryIsbn10
     * @param rankLastWeek
     * @param bestsellersDate
     * @param weeksOnList
     */
    public RanksHistory(String primaryIsbn10, String primaryIsbn13, Integer rank, String listName, String displayName, String publishedDate, String bestsellersDate, Integer weeksOnList, Integer rankLastWeek, Integer asterisk, Integer dagger) {
        super();
        this.primaryIsbn10 = primaryIsbn10;
        this.primaryIsbn13 = primaryIsbn13;
        this.rank = rank;
        this.listName = listName;
        this.displayName = displayName;
        this.publishedDate = publishedDate;
        this.bestsellersDate = bestsellersDate;
        this.weeksOnList = weeksOnList;
        this.rankLastWeek = rankLastWeek;
        this.asterisk = asterisk;
        this.dagger = dagger;
    }

    public String getPrimaryIsbn10() {
        return primaryIsbn10;
    }

    public void setPrimaryIsbn10(String primaryIsbn10) {
        this.primaryIsbn10 = primaryIsbn10;
    }

    public String getPrimaryIsbn13() {
        return primaryIsbn13;
    }

    public void setPrimaryIsbn13(String primaryIsbn13) {
        this.primaryIsbn13 = primaryIsbn13;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getBestsellersDate() {
        return bestsellersDate;
    }

    public void setBestsellersDate(String bestsellersDate) {
        this.bestsellersDate = bestsellersDate;
    }

    public Integer getWeeksOnList() {
        return weeksOnList;
    }

    public void setWeeksOnList(Integer weeksOnList) {
        this.weeksOnList = weeksOnList;
    }

    public Integer getRankLastWeek() {
        return rankLastWeek;
    }

    public void setRankLastWeek(Integer rankLastWeek) {
        this.rankLastWeek = rankLastWeek;
    }

    public Integer getAsterisk() {
        return asterisk;
    }

    public void setAsterisk(Integer asterisk) {
        this.asterisk = asterisk;
    }

    public Integer getDagger() {
        return dagger;
    }

    public void setDagger(Integer dagger) {
        this.dagger = dagger;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
