package com.nytimes.model.response;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultSeller {

    private String title;
    private String description;
    private String contributor;
    private String author;
    private String contributorNote;
    private String price;
    private String ageGroup;
    private String publisher;
    private List<Isbn> isbns = null;
    private List<RanksHistory> ranksHistory = null;
    private List<Review> reviews = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public ResultSeller() {
    }

    /**
     *
     * @param isbns
     * @param ranksHistory
     * @param contributor
     * @param reviews
     * @param author
     * @param contributorNote
     * @param price
     * @param description
     * @param publisher
     * @param ageGroup
     * @param title
     */
    public ResultSeller(String title, String description, String contributor, String author, String contributorNote, String price, String ageGroup, String publisher, List<Isbn> isbns, List<RanksHistory> ranksHistory, List<Review> reviews) {
        super();
        this.title = title;
        this.description = description;
        this.contributor = contributor;
        this.author = author;
        this.contributorNote = contributorNote;
        this.price = price;
        this.ageGroup = ageGroup;
        this.publisher = publisher;
        this.isbns = isbns;
        this.ranksHistory = ranksHistory;
        this.reviews = reviews;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContributor() {
        return contributor;
    }

    public void setContributor(String contributor) {
        this.contributor = contributor;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContributorNote() {
        return contributorNote;
    }

    public void setContributorNote(String contributorNote) {
        this.contributorNote = contributorNote;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public List<Isbn> getIsbns() {
        return isbns;
    }

    public void setIsbns(List<Isbn> isbns) {
        this.isbns = isbns;
    }

    public List<RanksHistory> getRanksHistory() {
        return ranksHistory;
    }

    public void setRanksHistory(List<RanksHistory> ranksHistory) {
        this.ranksHistory = ranksHistory;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
