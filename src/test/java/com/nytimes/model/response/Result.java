package com.nytimes.model.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {

    private String url;
    private String publicationDt;
    private String byline;
    @JsonProperty("book_title")
    private String bookTitle;
    @JsonProperty("book_author")
    private String bookAuthor;
    private String summary;
    private String uuid;
    private String uri;
    private List<String> isbn13 = null;

    private Result() {

    }

    public Result(String url, String publicationDt, String byline, String bookTitle, String bookAuthor, String summary, String uuid, String uri, List<String> isbn13) {
        super();
        this.url = url;
        this.publicationDt = publicationDt;
        this.byline = byline;
        this.bookTitle = bookTitle;
        this.bookAuthor = bookAuthor;
        this.summary = summary;
        this.uuid = uuid;
        this.uri = uri;
        this.isbn13 = isbn13;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPublicationDt() {
        return publicationDt;
    }

    public void setPublicationDt(String publicationDt) {
        this.publicationDt = publicationDt;
    }

    public String getByline() {
        return byline;
    }

    public void setByline(String byline) {
        this.byline = byline;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public List<String> getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(List<String> isbn13) {
        this.isbn13 = isbn13;
    }


}