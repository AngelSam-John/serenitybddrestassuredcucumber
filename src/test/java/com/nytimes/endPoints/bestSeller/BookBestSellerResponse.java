package com.nytimes.endPoints.bestSeller;

import com.nytimes.endPoints.bookReview.BookReviewResponse;
import com.nytimes.model.response.BookBestSeller;
import com.nytimes.model.response.BookReview;
import com.nytimes.model.response.RanksHistory;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookBestSellerResponse {

    private static final Logger logger = LoggerFactory.getLogger(BookReviewResponse.class);
    BookBestSeller bookBestSeller;
    RanksHistory ranksHistory;


    @Step("Get Book author from response")
    public String getAuthor(Response bookBestSellerResponse) {
        logger.info("Getting Book Author from the response! ");
        bookBestSeller = bookBestSellerResponse.getBody().as(BookBestSeller.class);
        return bookBestSeller.getResults().get(0).getAuthor();
    }
}
