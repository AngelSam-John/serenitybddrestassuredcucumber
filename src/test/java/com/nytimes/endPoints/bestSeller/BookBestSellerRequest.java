package com.nytimes.endPoints.bestSeller;

import com.nytimes.common.CommonRequestSpecification;
import com.nytimes.common.ConfigReader;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BookBestSellerRequest {

    private static final Logger logger = LoggerFactory.getLogger(BookBestSellerRequest.class);

    @Steps
    CommonRequestSpecification commonRequestSpecification;

    @Step("User makes a request to BookSeller endpoint with author {0} publisher {1} title {2}")
    public Response getBookBestSeller(String author, String publisher, String title,String apikey) {
        logger.info("Requesting the bestSeller for a book with book details !!!");
        return commonRequestSpecification.getBookBestSellerSpecification()
                .queryParam("author", author)
                .queryParam("publisher", publisher)
                .queryParam("title", title)
                .queryParam("api-key", apikey)
                .get();
    }

}
