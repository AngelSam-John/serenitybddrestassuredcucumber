package com.nytimes.endPoints.bookReview;

import com.nytimes.common.CommonRequestSpecification;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;
import com.nytimes.common.ConfigReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BookReviewRequest {

    ConfigReader configReader = new ConfigReader();
    String apiKey = configReader.getAPIKey();
    private static final Logger logger = LoggerFactory.getLogger(BookReviewRequest.class);
    CommonRequestSpecification commonRequestSpecification = new CommonRequestSpecification();


    @Step("User makes a request to review endpoint with isbn {0} title {1} author {2}")
    public Response getBookReview(String isbn, String title, String author) {
        logger.info("Requesting the Review for a book with book details !!!");
        return commonRequestSpecification.getBookReviewsSpecification()
                .queryParam("isbn", isbn)
                .queryParam("title", title)
                .queryParam("author", author)
                .queryParam("api-key", apiKey)
                .get();
    }
}

