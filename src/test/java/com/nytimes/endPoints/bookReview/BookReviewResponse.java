package com.nytimes.endPoints.bookReview;

import com.nytimes.model.response.BookReview;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookReviewResponse {

    private static final Logger logger = LoggerFactory.getLogger(BookReviewResponse.class);
    BookReview bookReview;

    @Step("Get Book title from response")
    public String getTitle(Response bookReviewResponse) {
        logger.info("Getting Book Title from the response! ");
        bookReview = bookReviewResponse.getBody().as(BookReview.class);
        return bookReview.getResults().get(0).getBookTitle();
    }

    @Step("Get Book summary from response")
    public String getSummary(Response bookReviewResponse) {
        logger.info("Getting Book Summary from the response! ");
        bookReview = bookReviewResponse.getBody().as(BookReview.class);
        return bookReview.getResults().get(0).getSummary();
    }

    @Step("Get Book author from response")
    public String getAuthor(Response bookReviewResponse) {
        logger.info("Getting Book Author from the response! ");
        bookReview = bookReviewResponse.getBody().as(BookReview.class);
        return bookReview.getResults().get(0).getBookAuthor();
    }

}
