Feature: Best Seller of a book

  @Sanity @getBookBestSeller
  Scenario:Get the best seller of a book
    Given Book BestSeller API is available to user
    When User requests for best seller of a book with "valid" key
      | Author         | Publisher                | Title    |
      | Sophia Amoruso | Portfolio/Penguin/Putnam | GIRLBOSS |
    Then User receives Best Seller of the given book with author
      | Author         |
      | Sophia Amoruso |
    Then User validates response code is 200
    Then User validates response schema matches with the specification defined in "book_bestseller_schema.json"

  @Sanity @getBookBestSellerWithInvalidApikey
  Scenario: Validate error response when invalid apikey is given
    Given Book BestSeller API is available to user
    When User requests for best seller of a book with "invalid" key
      | Author         | Publisher                | Title    |
      | Sophia Amoruso | Portfolio/Penguin/Putnam | GIRLBOSS |
    Then User validates response code is 401

