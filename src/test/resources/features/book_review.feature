Feature: Book Review Scenarios

  @Sanity @ValidateBookReview
  Scenario: User should be able to retrieve the book reviews
    Given Book API is available to user
    When User requests review endpoint with book details
      | ISBN          | Title                      | Author         |
      | 9780062797155 | The Tattooist of Auschwitz | Heather Morris |
    Then User validates response code is 200
    Then User validates title is present in the response
      | Title                      |
      | The Tattooist of Auschwitz |
    Then User validates author is present in the response
      | Author         |
      | Heather Morris |
    And User validates the response has summary details
    Then User validates response schema matches with the specification defined in "book_review_schema.json"


  @Sanity @ValidateBookReviewWithInvalidISBN
  Scenario: User should get a error response when request is made with invalid ISBN
    Given Book API is available to user
    When User requests review endpoint with book details
      | ISBN          | Title                      | Author         |
      | 9780307476462 | The Tattooist of Auschwitz | Heather Morris |
    Then User validates response code is 400


