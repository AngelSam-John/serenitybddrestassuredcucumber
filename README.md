# RestAPISerenityCucumber Framework

This is a Rest API test solution for endpoints available in https://any-api.com/nytimes_com/books_api/docs/API_Description . The published APIs represent a book system where user can get Book Review details and book BestSeller details.

Tests are written using a combination of SerenityBDD, RestAssured, Cucumber, Junit & Maven.

## Technology Stack

- Java
- Serenity BDD
- Maven
- RestAssured

## Prerequisites

* [Java 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java Dev Kit
* [Maven](https://maven.apache.org/download.cgi) - Dependency Manager

## Application Under Test

We are using nytimes_com/books_api APIs as the Application Under Test.

* URL : https://api.nytimes.com/svc/books/v3

## The project directory structure
The project follows the standard directory structure used in most Serenity projects:

```Gherkin
src
  + test
    + java  
      + com  
        + nytimes                          
          + commonUtils           Common utility methods                         
          + endpoints             methods to get endpoints request and response of the services
          + model                 pojos of all endpoints
          + runner                test runner(serenity runner/trigger configurations)
          + stepDefinitions       Step definitions for the BDD feature
    + resources
      + configuration             properties files
      + features                  Feature files
      + schema                    Schema files
      + logback-test.xml          logs configuration
      + serenity.conf             basic configuration file
```
Following instructions will help you running the project. First, clone this project locally on your machine from the master branch.

### Installation and Test Execution

Open the project in any IDE Eclipse/IntelliJ. Run the following command in Terminal and build the project. It will automatically download all the required dependencies.

```sh
$ mvn clean install
```

### Execute Tests

Run the following command in Terminal to execute tests.

```sh
$ mvn clean verify
```

### Test Report

You can find the Serenity reports in the following directory of the Project.

```sh
\target\site\serenity\
```

In the serenity directory, open 'index.html' file to view the report.

### EndPoints Interactions

Request the book reviews with the help of /reviews.json endpoint, and the book reviews will be displayed using reviews.json?isbn=""&title=""author=""&api-key=""

Request the bestseller of a book with the help of /lists/best-sellers/history.json endpoint, and the best-seller of the requested book will be displayed using /lists/best-sellers/history.json?author=""&publisher=""&title=""&api-key=""
